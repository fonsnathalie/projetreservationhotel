//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WpfReservationHotel.models
{
    using System;
    using System.Collections.Generic;
    
    public partial class OptionServiceLigneReservation
    {
        public int id { get; set; }
        public int id_OptionService { get; set; }
        public int id_LigneReservation { get; set; }
        public System.DateTime dateService { get; set; }
        public int nbPersonne { get; set; }
        public Nullable<double> prixTotalService { get; set; }
    
        public virtual LigneReservation LigneReservation { get; set; }
        public virtual OptionService OptionService { get; set; }
    }
}
