﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfReservationHotel.serveur
{
    class ServiceBase
    {
        //protected SqlConnection connection = null;
        protected string chaineConnection = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BDDRESERVATIONHOTEL;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        protected bool OpenConnection(SqlConnection connection)
        {
            try
            {
               
                connection.Open();

                return true;
            }
            catch(Exception e)
            {
                connection = null;
                MessageBox.Show(e.Message);
                return false;
            }

        }

        protected void Disconnect(SqlConnection connection)
        {
            try
            {
                using (connection)
                {
                    connection.Close();
                }
            }
            catch
            {
                connection = null;
            }

        }
    }
}
