﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfReservationHotel.models;

namespace WpfReservationHotel.serveur
{
    class ServiceClients: ServiceBase
    {

        #region Singleton



        private static ServiceClients _instance = null;
        public static ServiceClients Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ServiceClients();
                }
                return _instance;

            }
        }

        //empeche instanciation de la classe
        private ServiceClients()
        {

        }

        #endregion

        public Client ChargerClient(int id)
        {
            Client client = new Client(); 
            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return client;
            using (connection) {
                string request = "select id, nom, prenom, email, tel, adresse from Client   where id = @id";
                SqlCommand command = new SqlCommand(request, connection);
                using (command) {
                    command.Parameters.Add(new SqlParameter("@id", id));

                    SqlDataReader reader = command.ExecuteReader();

                    using (reader) {
                        while (reader.Read())
                        {
                            InitClient(client, reader);
                        }
                    }
                    reader.Dispose();
                }command.Dispose();
            }
                
            
            Disconnect(connection);
            return client;
        }
        public List<Client> ChargerClients()
        {
            List<Client> list = new List<Client>();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return list;

            string request = "select id, nom, prenom, email, tel, adresse from Client";
                SqlCommand command = new SqlCommand(request, connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Client client = new Client();
                    InitClient(client, reader);
                    list.Add(client);
                }

            Disconnect(connection);
                    return list;


        }


        private Client InitClient(Client client, SqlDataReader reader)
        {
            client.nom = reader.GetString(1);
            client.prenom = reader.GetString(2);
            client.adresse = reader.GetString(5);
            client.email = reader.GetString(3);
            client.tel = reader.GetString(4);
            client.id = reader.GetInt32(0);
            return client;
        }




        public int  sauvegarderClient(Client client)
        {
            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return 0;
            string request;
            if (client.id == 0)
            {
                request = "insert into Client(nom, prenom, email, tel, adresse) values(@nom, @prenom, @email, @tel, @adresse)";



            }
            else
            {
                request = "update Client set nom = @nom, @prenom= prenom, email = @email, tel =  @tel, adresse= @adresse where id=@id";

            }
            SqlCommand command = new SqlCommand(request, connection);
            command.Parameters.Add(new SqlParameter("@nom", client.nom));
            command.Parameters.Add(new SqlParameter("@prenom", client.prenom));
            command.Parameters.Add(new SqlParameter("@adresse", client.adresse));
            command.Parameters.Add(new SqlParameter("@email", client.email));
            command.Parameters.Add(new SqlParameter("@tel", client.tel));
            if (client.id != 0)
            {
                command.Parameters.Add(new SqlParameter("@id", client.id));
            }
            


                command.ExecuteNonQuery();
            Disconnect(connection);
            return client.id;
        }

        public bool  supprimerClient(Client client)
        {
            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return false;

                string request = "delete from Client where id=@id";
                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@id", client.id));

            command.ExecuteNonQuery();

            Disconnect(connection);
            return true;
        }

        public List<Client> ChargerClientsAvec(string nom, string prenom) {
            List<Client> listclients = new List<Client>();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return listclients;

            if (string.IsNullOrWhiteSpace(nom) || string.IsNullOrWhiteSpace(prenom))
            {
                if (string.IsNullOrWhiteSpace(nom))
                {
                    ChargerClientsAvecPrenom(prenom);
                }
                else
                {
                    ChargerClientsAvecNom(nom);
                }
            }
            else
            {
                string request = "select id, nom, prenom, email, tel, adresse from Client where nom = @nom and prenom = @prenom;";
                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@nom", nom));
                command.Parameters.Add(new SqlParameter("@prenom", prenom));

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Client client = new Client();
                    InitClient(client, reader);
                    listclients.Add(client);
                }
                
            }

            Disconnect(connection);

            
            return listclients;
        }

        public List<Client> ChargerClientsAvecNom(string nom)
        {
            List<Client> listclients = new List<Client>();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return listclients;

            string request = "select id, nom, prenom, email, tel, adresse from Client where nom = @nom";
            SqlCommand command = new SqlCommand(request, connection);
            command.Parameters.Add(new SqlParameter("@nom", nom));
            

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Client client = new Client();
                InitClient(client, reader);
                listclients.Add(client);
            }


            Disconnect(connection);
            return listclients;


        }

        public List<Client> ChargerClientsAvecPrenom(string prenom)
        {
            List<Client> listclients = new List<Client>();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return listclients;

            string request = "select id, nom, prenom, email, tel, adresse from Client where prenom = @prenom";
            SqlCommand command = new SqlCommand(request, connection);
            
            command.Parameters.Add(new SqlParameter("@prenom", prenom));

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Client client = new Client();
                InitClient(client, reader);
                listclients.Add(client);
            }


            Disconnect(connection);
            return listclients;


        }

    }


}
