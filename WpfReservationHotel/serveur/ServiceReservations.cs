﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfReservationHotel.models;

namespace WpfReservationHotel.serveur
{
    class ServiceReservations: ServiceBase
    {
        #region Singleton



        private static ServiceReservations _instance = null;
        public static ServiceReservations Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ServiceReservations();
                }
                return _instance;

            }
        }

        //empeche instanciation de la classe
        private ServiceReservations()
        {

        }

        #endregion

        public List<Reservation> ChargerReservations()
        {
            List<Reservation> reservations = new List<Reservation>();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return reservations;
            using (connection) {
                string request = "select id, dateDebut,  dateFin,  nbClient, libelleFacture, id_Client, id_MoyenPaiement from Reservation order by dateDebut desc";
                SqlCommand command = new SqlCommand(request, connection);
                using (command)
                {
                    //command.Parameters.Add(new SqlParameter("@id", id));
                    SqlDataReader reader = command.ExecuteReader();
                    using (reader) {
                        while (reader.Read())
                        {
                            Reservation reservation = new Reservation();
                            InitReservation(reservation, reader);
                            reservations.Add(reservation);
                        }
                    }reader.Dispose();
                        
                }command.Dispose();
                   
            }

            
            Disconnect(connection);
            return reservations;


        }


        private void InitReservation(Reservation reservation, SqlDataReader reader)
        {
            reservation.id = reader.GetInt32(0);
            reservation.dateDebut = reader.GetDateTime(1);
            reservation.dateFin = reader.GetDateTime(2);
            reservation.nbClient = reader.GetInt32(3);
            reservation.libelleFacture = reader.GetString(4);
            reservation.id_Client = reader.GetInt32(5);
            reservation.id_MoyenPaiement = reader.GetInt32(6);
            reservation.LigneReservation = this.ChargerLignesReservation(reservation.id);
            
        }


        public MoyenPaiement ChargerMoyenDePaiement(int id) {
            MoyenPaiement moyenpaiement = new MoyenPaiement();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return moyenpaiement;
            using (connection)
            {
                string request = "select id, libelleMoyenPaiement from MoyenPaiement where id =@id";
                SqlCommand command = new SqlCommand(request, connection);
                
                using (command)
                {
                    //command.Parameters.Add(new SqlParameter("@id", id));
                    SqlDataReader reader = command.ExecuteReader();
                    using (reader)
                    {
                        while (reader.Read())
                        {
                            moyenpaiement.id = reader.GetInt32(0);
                            moyenpaiement.libelleMoyenPaiement = reader.GetString(1);
                        }
                    }
                    reader.Dispose();

                }
                command.Dispose();

            }


            Disconnect(connection);

            return moyenpaiement;
        }
        

        public Collection<LigneReservation> ChargerLignesReservation(int id_reservation) { 
        
            Collection<LigneReservation> listLignesReservation = new Collection<LigneReservation>();
            List<Reservation> reservations = new List<Reservation>();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return listLignesReservation;
            using (connection)
            {
                string request = "select id, prixTotalLigne,numero,  id_Reservation from LigneReservation where id_Reservation = @idReservation";
                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@idReservation", id_reservation));
                using (command)
                {
                    SqlDataReader reader = command.ExecuteReader();
                    using (reader)
                    {
                        while (reader.Read())
                            {
                            LigneReservation lignereservation = new LigneReservation();
                            InitLigneReservation(lignereservation, reader);
                            listLignesReservation.Add(lignereservation);
                        }
                    }
                    reader.Dispose();

                }
                command.Dispose();

            }


            Disconnect(connection);
            return listLignesReservation;
        }

        private void InitLigneReservation(LigneReservation lignereservation, SqlDataReader reader)
        {
            lignereservation.id = reader.GetInt32(0);
            lignereservation.prixTotalLigne = reader.GetDouble(1);
            lignereservation.numero = reader.GetInt32(2);
            lignereservation.id_Reservation = reader.GetInt32(3);
            lignereservation.Chambre = ServiceChambres.Instance.ChargerChambre(lignereservation.numero);
            lignereservation.OptionServiceLigneReservation = ChargerOptionServiceLignesReservation(lignereservation.id);

        }

        public Collection<OptionServiceLigneReservation> ChargerOptionServiceLignesReservation(int id_ligne_reservation)
        {

            Collection<OptionServiceLigneReservation> listOptionServiceLignesReservation = new Collection<OptionServiceLigneReservation>();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return listOptionServiceLignesReservation;
            using (connection)
            {
                string request = "select id, id_OptionService,id_LigneReservation,  dateService, nbPersonne, prixTotalService from OptionServiceLigneReservation where id_LigneReservation = @idLigneReservation";
                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@idLigneReservation", id_ligne_reservation));
                using (command)
                {
                    //command.Parameters.Add(new SqlParameter("@id", id));
                    SqlDataReader reader = command.ExecuteReader();
                    using (reader)
                    {
                        while (reader.Read())
                        {
                            OptionServiceLigneReservation optionlignereservation = new OptionServiceLigneReservation();
                            InitOptionLigneReservation(optionlignereservation, reader);
                            listOptionServiceLignesReservation.Add(optionlignereservation);
                        }
                    }
                    reader.Dispose();

                }
                command.Dispose();
            }

            Disconnect(connection);

                return listOptionServiceLignesReservation;
        }

        private void InitOptionLigneReservation(OptionServiceLigneReservation optionlignereservation, SqlDataReader reader)
        {
            optionlignereservation.id = reader.GetInt32(0);
            optionlignereservation.id_OptionService = reader.GetInt32(1);
            optionlignereservation.id_LigneReservation = reader.GetInt32(2);
            optionlignereservation.dateService = reader.GetDateTime(3);
            optionlignereservation.nbPersonne = reader.GetInt32(4);
            optionlignereservation.prixTotalService = reader.GetDouble(5);
            if (optionlignereservation.id_OptionService >= 0) {
                optionlignereservation.OptionService = ChargerOptionService(optionlignereservation.id_OptionService);
            }

        }
        public OptionService ChargerOptionService(int id_option)
        {

            OptionService option = new OptionService();

            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return option;
            using (connection)
            {
                string request = "select id, libelle,prix,  prixPromo from OptionService where id = @idOption";
                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@idOption", id_option));
                using (command)
                {
                    //command.Parameters.Add(new SqlParameter("@id", id));
                    SqlDataReader reader = command.ExecuteReader();
                    using (reader)
                    {
                        while (reader.Read())
                        {   

                            InitOptionService(option, reader);
                        }
                    }
                    reader.Dispose();

                }
                command.Dispose();
            }

            Disconnect(connection);

            return option;
        }


        private void InitOptionService(OptionService optionservice, SqlDataReader reader)
        {
            optionservice.id = reader.GetInt32(0);
            optionservice.libelle = reader.GetString(1);
            optionservice.prix = reader.GetDouble(2);
            optionservice.prixPromo = reader.GetDouble(3);
        }
        public int sauvegarderReservation(Reservation reservation) {


            return 0;
        }

        public bool supprimerReservation(Reservation reservation) {

            SqlConnection connection = new SqlConnection(chaineConnection);
            if (OpenConnection(connection) == false) return false;

            if (reservation.id != 0)
            {

                foreach (LigneReservation ligneresa in reservation.LigneReservation) {
                    foreach (OptionServiceLigneReservation ligneoption in ligneresa.OptionServiceLigneReservation) {
                        supprimerOptionServiceLigneReservation(ligneoption.id);
                   }
                    supprimerLigneReservation(ligneresa.id);
                }

                string request = "delete from Reservation where id = @id;";

                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@id", reservation.id));
                command.ExecuteNonQuery();
            }
            Disconnect(connection);

            return true;
        }

        public bool supprimerLigneReservation(int idLigneresa)
        {

            SqlConnection connection = new SqlConnection(chaineConnection);
            if (OpenConnection(connection) == false) return false;

            if (idLigneresa != 0)
            {

                string request = "delete from LigneReservation where id = @id; ";

                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@id", idLigneresa));
                command.ExecuteNonQuery();
            }
            Disconnect(connection);

            return true;
        }

        public bool supprimerOptionServiceLigneReservation(int idLigneoption)
        {

            SqlConnection connection = new SqlConnection(chaineConnection);
            if (OpenConnection(connection) == false) return false;

            if (idLigneoption != 0)
            {

                string request = "delete from OptionServiceLigneReservation where id = @id; ";

                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@id", idLigneoption));
                command.ExecuteNonQuery();
            }
            Disconnect(connection);

            return true;
        }

    }
}
