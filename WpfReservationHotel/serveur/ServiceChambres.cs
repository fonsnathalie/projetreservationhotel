﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfReservationHotel.models;
using static WpfReservationHotel.viewmodels.ViewModelChambre;




namespace WpfReservationHotel.serveur
{
    class ServiceChambres: ServiceBase
    {

        #region Singleton



        private static ServiceChambres _instance = null;
        public static ServiceChambres Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ServiceChambres();
                }
                return _instance;

            }
        }

        //empeche instanciation de la classe
        private ServiceChambres()
        {

        }

        #endregion

 
        //classe qui prépare la liste pour afficher le statut de nettoyage des chambre - planning menage
        public List<Menage> ChargerChambres()
        {
            List<Menage> liste = new List<Menage>();
            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return liste;
            string request = "select numero, case when cast(dateNettoyageProchain as date) <= cast(getdate() as date)  then 'à faire' " +
                "else 'fait' end as statutmenage from chambre ORDER BY statutmenage ASC";
            SqlCommand command = new SqlCommand(request, connection);

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Menage menage ;
                int numeroChambre = reader.GetInt32(0);
                string statutmenage = reader.GetString(1);
                menage = new Menage(numeroChambre, statutmenage);

                liste.Add(menage);
            }
            Disconnect(connection);



            return liste;

        }

        
        


        //SaveDateMenage

        
        public int SaveDateMenage(Chambre chambre)
        {
            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return 0;
            string request;
            using (connection)
            if(chambre.numero == 0){
                request = "insert into Chambre(numero, capacite, prixUneNuit, prixCinqNuit, prixDixNuit, litBebe, balcon, coursInterieure, prochainMenage, nombreLitsDoubles, nombreLitsSimples)"
                   + "values(@numero, @capacite, @prixUneNuit, @prixCinqNuit, @prixDixNuit)";



            }
            else
            {
                request = "update chambre set (capacite= @capacite, prixUneNuit = @prixUneNuit, prixCinqNuit =  @prixCinqNuit, prixDixNuit= @prixDixNuit,"
                    + "litBebe = @litBebe, balcon = @balcon, coursInterieure = @coursInterieure, prochainMenage = @prochainMenage, nombreLitsDoubles = @nombreLitsDoubles, nombreLitsSimples = @nombreLitsSimples,"
                    + " where numero = @numero";

            }
            SqlCommand command = new SqlCommand(request, connection);
            command.Parameters.Add(new MySqlParameter("@numero", chambre.numero));
            command.Parameters.Add(new MySqlParameter("@capacite", chambre.capacite));
            command.Parameters.Add(new MySqlParameter("@prixUneNuit", chambre.prixUneNuit));
            command.Parameters.Add(new MySqlParameter("@prixCinqNuit", chambre.prixCinqNuit));
            command.Parameters.Add(new MySqlParameter("@prixDixNuit", chambre.prixDixNuit));
            command.Parameters.Add(new MySqlParameter("@litBebe", chambre.litBebe));
            command.Parameters.Add(new MySqlParameter("@balcon", chambre.balcon));
            command.Parameters.Add(new MySqlParameter("@coursInterieure", chambre.coursInterieure));
            command.Parameters.Add(new MySqlParameter("@dateNettoyageRealise", chambre.dateNettoyageRealise));
            command.Parameters.Add(new MySqlParameter("@dateNettoyageProchain", chambre.dateNettoyageProchain));
            

            command.ExecuteNonQuery();
            Disconnect(connection);
            return chambre.numero;
        }

  
        //updateStatutMenage
        //met à jour la date de ménage réalisé et calcule la date de ménage à réaliser
        public bool updateStatutMenage(Menage menage)
        {
            SqlConnection connection = new SqlConnection(chaineConnection);
            if (OpenConnection(connection) == false) return false;

            if (menage.numero != 0)
            {
                Console.WriteLine(menage.numero);
                string request = "update chambre set dateNettoyageRealise = getdate() where numero = @numero; " +
                    "update chambre set dateNettoyageProchain = (select case when(datediff(day, r.datedebut, r.datefin))" +
                    " < 4 then(c.dateNettoyageRealise + 1) when(datediff(day, r.datedebut, r.datefin)) > 3 then(dateNettoyageRealise + 3) end " +
                    "from chambre c inner join LigneReservation lr on c.numero = lr.numero inner join Reservation r on r.id = lr.id_Reservation " +
                    "where c.numero = @numero and  dateNettoyageRealise between r.datedebut and r.datefin) where numero = @numero;";

                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@numero", menage.numero));
                command.ExecuteNonQuery();
            }
            Disconnect(connection);

            return true;
        }

        

        //retourne la liste des  chambres disponible à la date d'aujourd'hui
        public List<Chambre> ChargerChambresDisponibles(DateTime dateDebut, DateTime dateFin) {
            List<Chambre> listeChambresDispos = new List<Chambre>();
            SqlConnection connection = new SqlConnection(chaineConnection);
            if (OpenConnection(connection) == false) return listeChambresDispos;
           
            string request = "select c.numero, capacite, prixUneNuit, prixCinqNuit, prixDixNuit, litBebe, balcon, coursInterieure  " +
                "dateNettoyageProchain, dateNettoyageProchain, dateNettoyageRealise from chambre c inner join LigneReservation lr " +
                "on c.numero = lr.numero inner join Reservation r on lr.id_Reservation = r.id " +
                "where c.numero not in(select c.numero from chambre inner join LigneReservation lr on c.numero = lr.numero " +
                "inner join Reservation r on lr.id_Reservation = r.id " +
                "where getdate() between @dateDebut and @dateFin)";

                SqlCommand command = new SqlCommand(request, connection);
                command.Parameters.Add(new SqlParameter("@dateDebut", dateDebut));
                command.Parameters.Add(new SqlParameter("@dateFin", dateFin));

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Chambre chambre = new Chambre();
                InitChambre(chambre, reader);
                listeChambresDispos.Add(chambre);
            }

            Disconnect(connection);
            return listeChambresDispos;
        }


        public Chambre ChargerChambre(int numero)
        {

            Chambre chambre = new Chambre();
            SqlConnection connection = new SqlConnection(chaineConnection);
            if (OpenConnection(connection) == false) return chambre;


            string request = "select numero, capacite, prixUneNuit, prixCinqNuit, prixDixNuit, litBebe, balcon, coursInterieure  " +
                "dateNettoyageProchain, dateNettoyageProchain, dateNettoyageRealise from chambre where numero=@numero";

            SqlCommand command = new SqlCommand(request, connection);
            command.Parameters.Add(new SqlParameter("@numero", numero));

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {

                InitChambre(chambre, reader);
                return chambre;
            }
 
            Disconnect(connection);
            return chambre;
        }

       

        private Chambre InitChambre(Chambre chambre, SqlDataReader reader)
        {
            chambre.numero = reader.GetInt32(0);
            chambre.capacite = reader.GetInt32(1);
            chambre.prixUneNuit = reader.GetDouble(2);
            chambre.prixCinqNuit = reader.GetDouble(3);
            chambre.prixDixNuit = reader.GetDouble(4);
            chambre.litBebe = reader.GetBoolean(5);
            chambre.balcon = reader.GetBoolean(6);
            chambre.coursInterieure = reader.GetBoolean(7);
            chambre.dateNettoyageProchain = reader.GetDateTime(8);
            if(chambre.dateNettoyageRealise != null) {
                chambre.dateNettoyageRealise = reader.GetDateTime(9);
           }
            

            return chambre;
        }


    }
}
