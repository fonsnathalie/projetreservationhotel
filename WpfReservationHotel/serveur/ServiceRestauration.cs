﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using static WpfReservationHotel.viewmodels.ViewModelRestauration;

namespace WpfReservationHotel.serveur
{
    class ServiceRestauration: ServiceBase
    {

        #region Singleton



        private static ServiceRestauration _instance = null;
        public static ServiceRestauration Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ServiceRestauration();
                }
                return _instance;

            }
        }

        //empeche instanciation de la classe
        private ServiceRestauration()
        {

        }

        #endregion

        public List<Restauration> ChargerRestauration()
        {
            DateTime date = DateTime.Now;
            DateTime[] tableaujour = new DateTime[7];
            int[] tableaunombre = new int[7] { 0, 0, 0, 0, 0, 0, 0 };
            List <DateTime> semaine = new List<DateTime>();
            
            for (int i=0;  i <= 6; i++)
            {
                DateTime jour = date.AddDays(i);
                semaine.Add(jour);
            }

            List <Restauration> list = new List<Restauration>();


            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return list;
            using (connection)
            {
                string request = "select dateService, SUM(nbPersonne) from OptionServiceLigneReservation where id_OptionService = 1 and  dateService between getdate() and DATEADD(DAY, +6, GETDATE()) group by dateService ";
                SqlCommand command = new SqlCommand(request, connection);
                SqlDataReader reader = command.ExecuteReader();

                // On rempli un tableau avec les dates de toute la semaine à compter d'aujourd'hui

                for (int i = 0; i <= 6; i++)
                {
                    tableaujour[i] = semaine[i];
                }


                while (reader.Read())
                    {

                        DateTime dateReader = reader.GetDateTime(0);
                        
                        int nbPersonne = reader.GetInt32(1);


                        // comparaison entre la date du reader et les dates du tableau contenant les jours
                        // Lorsque la date du reader correspond à une des dates du tableau contenant les jours, 
                        // on change la valeur du tableau contenant le nombre de personne au même indice que la date

                        for (int i = 0; i <= 6; i++)
                        {
                            if ( tableaujour[i].ToString("dd-MM-yyyy").Equals(dateReader.ToString("dd-MM-yyyy")))
                            {
                                tableaunombre[i] = nbPersonne;
                            }
                        }
                    }
                    // une fois toutes les valeurs du reader récupérées, on crée un objet par date avant de l'insérer dans la liste list
                    for(int i = 0; i <= 6; i++)
                    {
                        Restauration reservation = new Restauration(tableaujour[i].ToString("dd-MM-yyyy"), tableaunombre[i]);
                        list.Add(reservation);
                    }


                    
                
            }
            Disconnect(connection);
            list.ForEach(Console.WriteLine);
            return list;
            
        }
    }
}
