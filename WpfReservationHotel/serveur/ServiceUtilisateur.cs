﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfReservationHotel.models;

namespace WpfReservationHotel.serveur
{


    class ServiceUtilisateur: ServiceBase
    {

        #region Singleton



        private static ServiceUtilisateur _instance = null;
        public static ServiceUtilisateur Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ServiceUtilisateur();
                }
                return _instance;

            }
        }

        //empeche instanciation de la classe
        private ServiceUtilisateur()
        {

        }

        #endregion
        public RoleUtilisateur ChargerRole(int id) {
            RoleUtilisateur role = null; 
            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return role;
            using (connection) {
                string request = "select id, libelle from RoleUtilisateur where id=@id";
                SqlCommand command = new SqlCommand(request, connection);
                using (command) {
                    command.Parameters.Add(new SqlParameter("@id", id));
                    SqlDataReader reader = command.ExecuteReader();
                    using (reader) {
                        while (reader.Read())
                        {
                            role = new RoleUtilisateur();
                            InitRole(role, reader);
                        }
                    }reader.Dispose();
                        
                }command.Dispose();
                    
            }
            Disconnect(connection);
            return role;

        }
        public  Employe ChargerUtilisateur(string login) {

                Employe employe = new Employe(); 
            SqlConnection connection = new SqlConnection(chaineConnection);

            if (OpenConnection(connection) == false) return employe;
            using (connection)
            {
                string request = "select id, login, nom, prenom, mdp, email, id_RoleUtilisateur from Employe where login = @login";
                SqlCommand command = new SqlCommand(request, connection);
                using (command) {
                    command.Parameters.Add(new SqlParameter("@login", login));
                    SqlDataReader reader = command.ExecuteReader();
                    using (reader) {
                        while (reader.Read())
                        {
                            employe = new Employe();
                            InitEmploye(employe, reader);
                        }
                    }reader.Dispose();
                       
                }command.Dispose();
                    
            }
                
            Disconnect(connection);
            return employe;

        }

        private void InitRole(RoleUtilisateur role, SqlDataReader reader)
        {
            role.id = reader.GetInt32(0);
            role.libelle = reader.GetString(1);

        }

        private void InitEmploye(Employe employe, SqlDataReader reader) {
            employe.id = reader.GetInt32(0);
            employe.login = reader.GetString(1);
            employe.nom = reader.GetString(2);
            employe.prenom = reader.GetString(3);
            employe.mdp = reader.GetString(4);
            employe.email = reader.GetString(5);
            employe.id_RoleUtilisateur = reader.GetInt32(6);
        }

    }
}
