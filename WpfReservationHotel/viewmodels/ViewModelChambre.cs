﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfReservationHotel.models;
using WpfReservationHotel.serveur;

namespace WpfReservationHotel.viewmodels
{
    class ViewModelChambre : ViewModelBase
    {

       
        public class Menage
        {
            public int numero { get; set; }
            public string statutmenage { get; set; }

            public Menage(int num, string statut)
            {
                numero = num;
                statutmenage = statut;
            }
        }

        //les evenements
        
        public event EventHandler OnStatutChanged;
        public Menage Objetmenage { get; }
        public ViewModelChambre(Menage menage)
        {
            Objetmenage = menage;
        }

        public string NumeroStatut
        {
            
            get
            {
                return Objetmenage.numero + " " + Objetmenage.statutmenage;
                //OnPropertyChanged("NumeroStatut");
            }

        }


        public int Numero
        {
            get { return Objetmenage.numero; }
            set
            {
                Objetmenage.numero = value;
                OnPropertyChanged("Numero");
                OnPropertyChanged("NumeroStatut");
            }

        }      


            public String StatutMenage
        {
            get { return Objetmenage.statutmenage; }
            set{
                Objetmenage.statutmenage = value;              
                OnPropertyChanged("StatutMenage");
                OnPropertyChanged("NumeroStatut");
            }

        }


        private ICommand updateMenage;
        public ICommand UpdateMenage
        {
            get
            {
                if (updateMenage == null)
                {
                    updateMenage = new RelayCommand(sender => {

                        if (Objetmenage.numero > 0) ServiceChambres.Instance.updateStatutMenage(Objetmenage);
                        ChangerStatutMenageAfait();
                        OnStatutChanged?.Invoke(this, EventArgs.Empty);
                    });
                }
                return updateMenage;
            }
        }

        private void ChangerStatutMenageAfait() {

            this.StatutMenage = "fait";

        }

    }
}
