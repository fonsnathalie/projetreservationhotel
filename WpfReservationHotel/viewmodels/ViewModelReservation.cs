﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfReservationHotel.models;
using WpfReservationHotel.serveur;

namespace WpfReservationHotel.viewmodels
{
    public class ViewModelReservation : ViewModelBase
    {
       
        public ViewModelClient ViewModelClient { get; set; }

        public bool Edition { get; set; }

        public string TextBoutonSauvegarder { get; set; }

        public string TextBoutonSupprimer { get; set; }

        public event EventHandler OnReservationSupprime;
        public event EventHandler OnReservationSauvegarde;

        public Reservation Reservation { get; set; }

        public DateTime DateDebut
        {

            set {
                DateDebutAchange();
                DateDebut = value;
            }
            get
            {
                return this.Reservation.dateDebut;//.ToString("dd-MM-yyyy");
            }
        }

        public string DateDebutText
        {
            get { 
            return DateDebut.ToString("dd-MM-yyyy");
            }
        }

        public DateTime DateFin
        {
            set
            {
                DateFin = value;
            }
            get
            {
                return this.Reservation.dateFin;//.ToString("dd-MM-yyyy");
            }
        }

        private MoyenPaiement moyenPaiement { get; set; }
        public MoyenPaiement MoyenPaiement
        {
            get
            {
                if (moyenPaiement == null) {
                    ChargerMoyenPaiement();
                }
                return moyenPaiement;
            }
        }

        private int NombrePersonnes { 
            get {
                return Reservation.nbClient;
            }


            set{
                Reservation.nbClient = value;
            } 
        
        }

        

        public List<Chambre> ListChambresDisponibles { get; set; }
        public List<Client> ResultatClientsRecherches { get; set; }
        public ICollection<LigneReservation> ListLignesDeReservation { 
            get { return this.Reservation.LigneReservation; } 
            set { ListLignesDeReservation = value; } 
        }

        private ICollection<OptionServiceLigneReservation> listLignesOptionsDeReservation { get; set; }
        public ICollection<OptionServiceLigneReservation> ListLignesOptionsDeReservation
        {
            get {

                if (listLignesOptionsDeReservation == null) {
                    listLignesOptionsDeReservation = new Collection<OptionServiceLigneReservation>();

                }
                return this.listLignesOptionsDeReservation; 
            }
        }


        public ViewModelReservation(Reservation aReservation, bool edition)
        {

            Console.WriteLine("CREATION RESAAAA");
            this.Reservation = aReservation;

            this.ResultatClientsRecherches = new List<Client>();
            this.ListChambresDisponibles = new List<Chambre>();
            if (Reservation.dateDebut == null) {
                //Blank Reservation
                this.Reservation.dateDebut = DateTime.Now;
                this.Reservation.dateFin = DateTime.Now;
                this.Reservation.nbClient = 0;
                this.Reservation.LigneReservation = new List<LigneReservation>();
                this.ViewModelClient = new ViewModelClient(new Client());
            }
            else {
                ChargerClient();
                //ChargerChambresDisponibles();

                Console.WriteLine("RESA ADDED");
                Console.WriteLine("nbClients: " + aReservation.nbClient);
                Console.WriteLine("datedebut: " + aReservation.dateDebut.ToString());
                Console.WriteLine("datefin: " + aReservation.dateFin.ToString());
            }
            Console.WriteLine("RESA Edition");
            this.Edition = edition;
            if (edition)
            {
                Console.WriteLine("RESA Edition1");
                this.TextBoutonSauvegarder = "Modifier";
                this.TextBoutonSupprimer = "Supprimer";
                

            }
            else {
                Console.WriteLine("RESA Edition2");
                this.TextBoutonSauvegarder = "Sauvegarder";
                this.TextBoutonSupprimer = "Annuler";

            }
            OnPropertyChanged("TextBoutonSauvegarder");
            OnPropertyChanged("TextBoutonSupprimer");
            Console.WriteLine("RESA EditionFIN");
            Console.WriteLine("CREATION RESAAAA FINNNNNNNN");

        }

        //ajouterChambreReservee
        // ajouterOption(int nombrePersonnes)

        private async void ChargerClient()
        {
            Client client = new Client();
            await Task.Run(action: () =>
            {
                client = ServiceClients.Instance.ChargerClient(this.Reservation.id_Client);
            });
            if (client != null) {
                this.ViewModelClient = new ViewModelClient(client);
                this.OnPropertyChanged("ViewModelClient");
            }

            Console.WriteLine("CLIENT RESA ADDED");
            Console.WriteLine("Nom client: " + client.nom);
            Console.WriteLine("Prenom client: " + client.prenom);

        }

        private async void ChargerMoyenPaiement() {
            MoyenPaiement unMoyenPaiement = new MoyenPaiement();
            await Task.Run(action: () =>
            {
                unMoyenPaiement = ServiceReservations.Instance.ChargerMoyenDePaiement(this.Reservation.id_MoyenPaiement);
            });
            this.moyenPaiement = unMoyenPaiement;
        }

        private async void ChargerClientsAvec(string nom, string prenom) {
            ResultatClientsRecherches = null;
            List<Client> clientsTrouves = new List<Client>();
            await Task.Run(action: () =>
            {
                clientsTrouves = ServiceClients.Instance.ChargerClientsAvec(nom,prenom);
            });
            ResultatClientsRecherches = clientsTrouves;        }
        
       
        private async void ChargerChambresDisponibles()
        {
            List<Chambre> listChambres = new List<Chambre>();
            await Task.Run(action: () =>
            {
                listChambres = ServiceChambres.Instance.ChargerChambresDisponibles(this.Reservation.dateDebut,this.Reservation.dateFin);
            });
            if (listChambres.Count > 0)
            {
                this.ListChambresDisponibles = listChambres;
                this.OnPropertyChanged("ListChambresDisponibles");
            }
            foreach (Chambre chambre in listChambres) {
                Console.WriteLine("CHAMBRE RESA ADDED");
                Console.WriteLine("Nnum chambre: " + chambre.numero);
            }


        }

       


        ICommand commandeSauvegarder;

        public ICommand CommandeSauvegarder 
        {
            get
            {
                if (commandeSauvegarder == null)
                {
                    return new RelayCommand((object sender) => {
                        this.SauvegarderReservation();
                    });
                }
                return commandeSauvegarder;
            }

        }

        public async void SauvegarderReservation()
        {
            await Task.Run(action: () =>
            {
                ServiceReservations.Instance.sauvegarderReservation(this.Reservation);
                ViewModelClient.sauvegarderClient();

            });
            this.OnReservationSupprime?.Invoke(this, EventArgs.Empty);
        }

        ICommand commandeSupprimer;

        public ICommand CommandeSupprimer
        {
            get
            {
                if (commandeSupprimer == null)
                {
                    return new RelayCommand((object sender) => {
                       
                            this.SupprimerReservation();
                        
                    });
                }
                return commandeSupprimer;
            }

        }

        public async void SupprimerReservation()
        {
            if (this.Edition == true)
            {
                await Task.Run(action: () =>
                {
                ServiceReservations.Instance.supprimerReservation(this.Reservation);//,chambresSelectionnees,options);

                });
                this.OnReservationSupprime?.Invoke(this, EventArgs.Empty);
            }
            
        }


        private void DateDebutAchange()
        {

        }

        private void DateFinAchange()
        {

        }

        private ICommand etatSelectionChambreDisponibleAchange { get; set; }
        public ICommand EtatSelectionChambreDisponibleAchange
        {
            get
            {
                if (etatSelectionChambreDisponibleAchange == null)
                {
                    etatSelectionChambreDisponibleAchange =  new RelayCommand((object sender) => {
                        this.ChangerSelectionChambre();
                    });
                }
                return etatSelectionChambreDisponibleAchange;
            }

        }

        private void ChangerSelectionChambre() { 
        
        }

        private ICommand etatSelectionBalconAchange { get; set; }
        public ICommand EtatSelectionBalconAchange
        {
            get
            {
                if (etatSelectionBalconAchange == null)
                {
                    etatSelectionBalconAchange = new RelayCommand((object sender) => {
                        this.ChangerSelectionBalcon();
                    });
                }
                return etatSelectionBalconAchange;
            }

        }

        private void ChangerSelectionBalcon()
        {

        }

        private ICommand etatSelectionLitBebeAchange { get; set; }
        public ICommand EtatSelectionLitBebeAchange
        {
            get
            {
                if (etatSelectionLitBebeAchange == null)
                {
                    etatSelectionLitBebeAchange = new RelayCommand((object sender) => {
                        this.ChangerSelectionLitBebe();
                    });
                }
                return etatSelectionLitBebeAchange;
            }

        }

        private void ChangerSelectionLitBebe()
        {

        }

        private ICommand etatSelectionCoursInterieureAchange { get; set; }
        public ICommand EtatSelectionCoursInterieureAchange
        {
            get
            {
                if (etatSelectionCoursInterieureAchange == null)
                {
                    etatSelectionCoursInterieureAchange = new RelayCommand((object sender) => {
                        this.ChangerSelectionCoursInterieure();
                    });
                }
                return etatSelectionCoursInterieureAchange;
            }

        }

        private void ChangerSelectionCoursInterieure()
        {

        }

        

    }
       
}
