﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfReservationHotel.models;

namespace WpfReservationHotel.viewmodels
{
    public class ViewModelEmploye
    {
        public Employe Employe { get;  }

        public ViewModelEmploye() {
            this.Employe = new Employe();
        }
    }
}
