﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfReservationHotel.viewmodels
{
    public class ViewModelMain
    {
        public ViewModelMenu ViewModelMenu { get; set; }
        public ViewModelConnexion ViewModelConnexion { get; set; }

        public UserControl CurrentUserControl { get; set; }
        public ViewModelBase CurrentViewModel { get; set; }
        public ViewModelEmploye Employe { get;  }

        public event EventHandler OnConnexionReussie;
        public event EventHandler OnDeconnexionReussie;

       
        public ViewModelMain(ViewModelMenu viewmodelMenu) {
            updateViewModelMenu(viewmodelMenu);
        }

        public void updateViewModelMenu(ViewModelMenu viewmodel)
        {
            this.ViewModelMenu = viewmodel;
            this.ViewModelMenu.OnDeconnexionReussie += ViewModelMenu_OnDeconnexionReussie;

        }

        public void updateViewModelConnexion(ViewModelConnexion viewmodel)
        {
            this.ViewModelConnexion = viewmodel;
            this.ViewModelConnexion.OnConnexionReussie += ViewModelConnexion_OnConnexionReussie;
        }

        private void ViewModelMenu_OnDeconnexionReussie(object sender, EventArgs e)
        {
            InjectionDependances.ExecOnUiThread(Application.Current, () =>
            {
                this.OnDeconnexionReussie?.Invoke(this, e);
            });
            
        }

        private void ViewModelConnexion_OnConnexionReussie(object sender, EventArgs e)
        {
            
            InjectionDependances.ExecOnUiThread(Application.Current, () =>
             {
                 string nom = this.ViewModelConnexion.EmployeFormulaire.nom;
                 string prenom = this.ViewModelConnexion.EmployeFormulaire.prenom;
                 this.ViewModelMenu.estConnecteAvec(nom + " " + prenom);

                 OnConnexionReussie?.Invoke(this, e);
             });

        }


        


        }
    }
