﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfReservationHotel.models;
using WpfReservationHotel.serveur;

namespace WpfReservationHotel.viewmodels
{


    public class ViewModelConnexion : ViewModelBase
    {
        const int CONNEXION_REUSSIE = 1;
        const int CONNEXION_ECHOUEE_MOTDEPASSE = 2;
        const int CONNEXION_ECHOUEE_LOGIN = 3;
        public Employe EmployeFormulaire { get; set; }

        public RoleUtilisateur RoleUtilisateur { get; set; }

        public event EventHandler OnConnexionReussie ;
        public ViewModelConnexion() {
            this.EmployeFormulaire = new Employe();
        }

        private ICommand commandeConnexion { get; set; }

        public ICommand CommandeConnexion
        {
            get
            {
                if (commandeConnexion == null)
                {
                    return new RelayCommand((object sender) => {
                        //SHOW SPINNER LOADING IMAGE
                        if ((EmployeFormulaire.login != null) && (EmployeFormulaire.mdp != null))
                        {
                            this.ChargerUtilisateur(EmployeFormulaire.login, EmployeFormulaire.mdp);

                        }
                        else {
                            MessageBox.Show("Merci de bien vouloir compléter tous les champs");
                        }


                    });
                }
                return commandeConnexion;
            }
        }

        private async void ChargerUtilisateur(string login,string motDePasse) {

             await Task.Run(action: () => {
                 this.RoleUtilisateur = new RoleUtilisateur();

                 this.RoleUtilisateur.libelle = this.ResourcesManager.GetString("roleAccueil");

                 //this.connexionAvecUtilisateur(this.EmployeFormulaire, CONNEXION_REUSSIE);

                 Employe employe = ServiceUtilisateur.Instance.ChargerUtilisateur(login);
                 if (employe.login != null)
                 {
                     this.EmployeFormulaire = employe;
                     RoleUtilisateur = ServiceUtilisateur.Instance.ChargerRole(EmployeFormulaire.id_RoleUtilisateur);
                     if (motDePasse.Equals(employe.mdp))
                     {
                         this.connexionAvecUtilisateur(this.EmployeFormulaire, CONNEXION_REUSSIE);
                     }
                     else
                     {
                         this.connexionAvecUtilisateur(this.EmployeFormulaire, CONNEXION_ECHOUEE_MOTDEPASSE); ;
                     }
                 }
                 else {
                     
                     this.connexionAvecUtilisateur(this.EmployeFormulaire, CONNEXION_ECHOUEE_LOGIN); ;

                 }


                 //STOP SHOWING SPINNER LOADING IMAGE

             });
        }
        private void connexionAvecUtilisateur(Employe employe, int error)
        {
            //VERIFICATIONUTILISATEUR EXISTE
            switch (error) {
                case CONNEXION_REUSSIE:
                    OnConnexionReussie?.Invoke(this, EventArgs.Empty);
                    break;
                case CONNEXION_ECHOUEE_LOGIN:
                    MessageBox.Show("Utilisateur introuvable");
                    break;
                case CONNEXION_ECHOUEE_MOTDEPASSE:
                    MessageBox.Show("Mot de passe érroné");
                    break;
            }
        }

        
    }
}
