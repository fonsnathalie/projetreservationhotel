﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WpfReservationHotel.serveur;
using WpfReservationHotel.models;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows;

namespace WpfReservationHotel.viewmodels
{
    class ViewModelClients: ViewModelBase
    {
        ServiceClients Service = ServiceClients.Instance;


        public ICollectionView Observer;
        public ObservableCollection<ViewModelClient> ListClients { get; set; }

        public ViewModelClient SelectedClient
        {
            get
            {
                return (ViewModelClient)Observer.CurrentItem;
            }
        }
        public ViewModelClients() {
            getClients();
            Observer = System.Windows.Data.CollectionViewSource.GetDefaultView(ListClients);
            Observer.CurrentChanged += Observer_CurrentChanged;
            Observer.MoveCurrentToFirst();
        }

        public void Observer_CurrentChanged(object sender, EventArgs e)
        {
            this.OnPropertyChanged("SelectedClient");

        }

        private async  void getClients()
        {
            List<Client> aList = new List<Client>();
            this.ListClients = new ObservableCollection<ViewModelClient>();
            await Task.Run(action: () =>
            {
                aList = Service.ChargerClients();
                
            });

            foreach (Client client in aList)
            {
                ViewModelClient clientVM = new ViewModelClient(client);
                clientVM.OnClientSupprime += ClientVM_OnClientSupprime;
                clientVM.OnClientSauvegarde += ClientVM_OnClientSauvegarde;
                this.ListClients.Add(clientVM);
            }
            this.OnPropertyChanged("ListClients");
            Observer.MoveCurrentToFirst();

        }

        private void ClientVM_OnClientSauvegarde(object sender, EventArgs e)
        {

            MessageBox.Show("Le client a bien été sauvegardé");
        }

        public void ClientVM_OnClientSupprime(object sender, EventArgs e)
        {
            this.ListClients.RemoveAt(Observer.CurrentPosition);
            this.OnPropertyChanged("ListClients");
            MessageBox.Show("Le client a bien été supprimé");
            Observer.MoveCurrentToFirst();
        }


        ICommand commandeAjouterClient { get; set; }

        public ICommand CommandeAjouterClient {
            get {
                if (commandeAjouterClient == null) {
                    commandeAjouterClient= new RelayCommand((object sender) => {
                        this.ajouterClient();
                    });
                }
                return commandeAjouterClient;
            }
        }

        private void ajouterClient() {
            Client client = new Client();
            ViewModelClient clientVM = new ViewModelClient(client);
            this.ListClients.Insert(0, clientVM);
            OnPropertyChanged("ListClients");
            Observer.MoveCurrentToFirst();
            
        }

    }

    
}
