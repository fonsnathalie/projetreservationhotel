﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using WpfReservationHotel.serveur;
using WpfReservationHotel.models;
using System.ComponentModel;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows;

namespace WpfReservationHotel.viewmodels
{

   
    public class ViewModelReservations : ViewModelBase
    {
        ServiceReservations Service = ServiceReservations.Instance;
        public ObservableCollection<ViewModelReservation> ListReservations { get; }

        public ICollectionView Observer; 
        public ViewModelReservation SelectedReservation
        {
            get
            {
                return (ViewModelReservation)Observer.CurrentItem;
            }
        }
        public ViewModelReservations() {
            Console.WriteLine("CREATION RESAS");
            this.ListReservations = new ObservableCollection<ViewModelReservation>();
            Observer = System.Windows.Data.CollectionViewSource.GetDefaultView(ListReservations);
            Observer.CurrentChanged += Observer_CurrentChanged;
            Observer.MoveCurrentToFirst();
            this.ChargerReservations();
            Console.WriteLine("CREATION RESAS FIN");
        }

        private async void ChargerReservations()
        {
            List<Reservation> listeReservations = new List<Reservation>();
            await Task.Run(action: () =>
            {
                List<Reservation> aListe = null;
                aListe = Service.ChargerReservations();
                foreach (Reservation reservation in aListe)
                {
                    listeReservations.Add(reservation);
                }
            });

            
            foreach (Reservation reservation in listeReservations) {
                ViewModelReservation reservationVM = new ViewModelReservation(reservation, true);
                reservationVM.OnReservationSauvegarde += ReservationVM_OnReservationSauvegarde;
                reservationVM.OnReservationSupprime += ReservationVM_OnReservationSupprime;
                this.ListReservations.Add(reservationVM);
                Console.WriteLine(" RESA ADDED IN ListReservations  ");
            }
            OnPropertyChanged("ListReservations");
            Observer.MoveCurrentToFirst();

        }

        public void Observer_CurrentChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("SelectedReservation");
        }

        private ICommand commandeNouvelleReservation { get; set; }
        public ICommand CommandeNouvelleReservation
        {
            get
            {
                if (commandeNouvelleReservation == null)
                {
                    commandeNouvelleReservation = new RelayCommand((object sender) => {
                        this.ajouterNouvelleReservation();
                    });
                }
                return commandeNouvelleReservation;
            }

        }

        public void ajouterNouvelleReservation() { 


            Client nouveauClient = new Client();
        ViewModelClient clientVM = new ViewModelClient(nouveauClient);
        Reservation nouvelleReservation = new Reservation();
        ViewModelReservation reservationVM = new ViewModelReservation(nouvelleReservation,false);
        reservationVM.OnReservationSauvegarde += ReservationVM_OnReservationSauvegarde;
        reservationVM.OnReservationSupprime += ReservationVM_OnReservationSupprime;
        this.ListReservations.Insert(0, reservationVM);
        OnPropertyChanged("ListClients"); 
            OnPropertyChanged("ListReservations");
            Observer.MoveCurrentToFirst();
            

        }

        private void ReservationVM_OnReservationSupprime(object sender, EventArgs e)
        {
            this.ListReservations.RemoveAt(Observer.CurrentPosition);
            this.OnPropertyChanged("ListReservations");
            MessageBox.Show("La réservation a bien été supprimée");
        }

        private void ReservationVM_OnReservationSauvegarde(object sender, EventArgs e)
        {
            MessageBox.Show("La réservation a bien été sauvegardée");
        }

        private ICommand commandeAfficherFiltreReservation;
        public ICommand CommandeAfficherFiltreReservation
        {
            get
            {
                if (commandeAfficherFiltreReservation == null)
                {
                    return new RelayCommand((object sender) => {
                        this.AfficherFiltreReservations();
                    });
                }
                return commandeAfficherFiltreReservation;
            }

        }

        public void AfficherFiltreReservations()
        {

        }

        private ICommand commandeFiltrerReservation;
        public ICommand CommandeFiltreReservation
        {
            get
            {
                if (commandeFiltrerReservation == null)
                {
                    return new RelayCommand((object sender) => {
                        this.FiltrerReservations();
                    });
                }
                return commandeFiltrerReservation;
            }

        }

        public void FiltrerReservations()
        {

        }


    }
}
