﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using WpfReservationHotel.models;
using WpfReservationHotel.serveur;
using static WpfReservationHotel.viewmodels.ViewModelChambre;

namespace WpfReservationHotel.viewmodels
{
    class ViewModelMenage : ViewModelBase
    {
        public ObservableCollection<ViewModelChambre> ListeChambres { get; set; }
        private ICollectionView observeur; //design pattern observateur pour recuperer les elements de la liste

        public ViewModelChambre ChambreSelected
        {
            get
            {
                return observeur.CurrentItem as ViewModelChambre;
            }
        }

        public ViewModelMenage()
        {
            //la liste des chambre avec statut de ménage est chargée depuis servicecharmbre
            List<Menage> liste = ServiceChambres.Instance.ChargerChambres();

            ListeChambres = new ObservableCollection<ViewModelChambre>();
            foreach (Menage menage in liste)


            {
                ViewModelChambre vm = new ViewModelChambre(menage);
               
                vm.OnStatutChanged += Observeur_MenageChanged;
                ListeChambres.Add(vm);

            }

            //relié observateur sur collection observée
            observeur = CollectionViewSource.GetDefaultView(ListeChambres);

            observeur.MoveCurrentToFirst();
            //+= tab tab
            observeur.CurrentChanged += Observeur_CurrentChanged;


        }

        //syncronise l'affichage lors de changement de statut de ménage
        private void Observeur_MenageChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("ListeChambres");
            OnPropertyChanged("ChambreSelected");
        }

        //syncronise l'affichage de chambre selectionné
        private void Observeur_CurrentChanged(object sender, EventArgs e)
        {
           
            OnPropertyChanged("ChambreSelected");
        }
    }

}
