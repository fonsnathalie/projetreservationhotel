﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WpfReservationHotel.viewmodels
{
    public class ViewModelMenu : ViewModelBase
    {

        private string nomhotel{get;set;}
        public string NomHotel { get
            {
                try
                {
                    if (nomhotel == null)
                    {
                        return this.ResourcesManager.GetString("nomHotel");
                    }
                    else {
                        return nomhotel;
                    }
                }
                catch (Exception e) {
                    return "HOTEL";
                }

                //(string)Application.Current.Resources["nomHotel"];
            } 
        }
        public event EventHandler OnDeconnexionReussie;
        public Visibility VisibiliteBouton { get; set; }
        public string NomPrenom { get; set; }
        public ViewModelMenu() {
            this.VisibiliteBouton = Visibility.Hidden;
        }


        private ICommand CommandeDeconnexion { get; set; }
        public ICommand Deconnexion
        {
            get {
                if (CommandeDeconnexion == null)
                {
                    return new RelayCommand((object sender) => {
                        this.seDeconnecter();
                    });
                }
                return CommandeDeconnexion;
            }

        }

        public void estConnecteAvec(string nomprenom) {
                this.NomPrenom = nomprenom;
            this.VisibiliteBouton = Visibility.Visible;
            OnPropertyChanged("NomPrenom");
            OnPropertyChanged("VisibiliteBouton");

        }



        public void seDeconnecter() {

                this.VisibiliteBouton = Visibility.Hidden;
                this.NomPrenom = "";
            OnPropertyChanged("NomPrenom");
            OnPropertyChanged("VisibiliteBouton");
            this.OnDeconnexionReussie?.Invoke(this, EventArgs.Empty);
           
            
    }
}
    
}
