﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfReservationHotel.models;
using WpfReservationHotel.serveur;

namespace WpfReservationHotel.viewmodels
{
    public class ViewModelClient : ViewModelBase
    {
        public Client Client { get; set; }

        public event EventHandler OnClientSupprime;
        public event EventHandler OnClientSauvegarde;
        public ViewModelClient(Client aclient) {
            this.Client = aclient;
            OnPropertyChanged("Client");
        }

        public string Nom{
            get{
               return Client.nom;
            }
            set{
                Client.nom = value;
                
                OnPropertyChanged("Client");
                OnPropertyChanged("Nom");
            }
           }

        public string Prenom
        {
            get
            {
                return Client.prenom;
            }
            set
            {
                Client.prenom = value;
                OnPropertyChanged("Client");
                OnPropertyChanged("Prenom");
            }
        }

        public string Tel
        {
            get
            {
                return Client.tel;
            }
            set
            {
                Client.tel = value;
                OnPropertyChanged("Client");
                OnPropertyChanged("Tel");
            }
        }

        public string Email
        {
            get
            {
                return Client.email;
            }
            set
            {
                Client.email = value;
                OnPropertyChanged("Client");
                OnPropertyChanged("Email");
            }
        }

        public string Adresse
        {
            get
            {
                return Client.adresse;
            }
            set
            {
                Client.adresse = value;
                OnPropertyChanged("Client");
                OnPropertyChanged("Adresse");
            }
        }

       

        private ICommand commandeSupprimerClient { get; set; }
        public ICommand CommandeSupprimerClient
        {
            get
            {
                if (commandeSupprimerClient == null)
                {
                    commandeSupprimerClient = new RelayCommand((object sender) => {
                        this.supprimerClient();
                    });
                }
                return commandeSupprimerClient;
            }

        }

        public async void supprimerClient()
        {
            await Task.Run(action: () =>
            {
                ServiceClients.Instance.supprimerClient(this.Client);

            });
            this.OnClientSupprime?.Invoke(this,EventArgs.Empty);
            
        }

        private ICommand commandeSauvegarderClient { get; set; }
        public ICommand CommandeSauvegarderClient
        {
            get
            {
                if (commandeSauvegarderClient == null)
                {
                    commandeSauvegarderClient = new RelayCommand((object sender) => {
                      
                            this.sauvegarderClient();
                        
                    });
                }
                return commandeSauvegarderClient;
            }


        }

        public async void sauvegarderClient()
        {
            await Task.Run(action: () =>
            {
              this.Client.id =  ServiceClients.Instance.sauvegarderClient(this.Client);

            });
            OnClientSauvegarde?.Invoke(this, EventArgs.Empty);

        }


        public bool isClientValid() {
            return true;
        }
    }
}
