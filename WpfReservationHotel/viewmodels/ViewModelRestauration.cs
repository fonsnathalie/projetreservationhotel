﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfReservationHotel.serveur;
using WpfReservationHotel.models;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace WpfReservationHotel.viewmodels
{
    class ViewModelRestauration : ViewModelBase
    {
        public class Restauration
        {
            public string day { get; set; }
            public int number { get; set; }

            public Restauration(string jour, int nbre)
            {
                day = jour;
                number = nbre;
            }
        }

        ServiceRestauration Service = ServiceRestauration.Instance;
        public List<Restauration> ListRestauration { get; set; }


        public ViewModelRestauration()
        {
            
            getReservations();
        }
        private void getReservations()
        {
            ListRestauration = new List<Restauration>();
            List<Restauration> aList;
            aList = Service.ChargerRestauration();
            foreach (Restauration reservation in aList)
            {
                ListRestauration.Add(reservation);
            }
            OnPropertyChanged("ListRestauration");
        }

        public void mettreajour()
        {
            getReservations();
        }

        public ICommand CommandMiseaJour
        {
            get
            {
                if(commandMiseaJour == null)
                {
                    return new RelayCommand((object sender) => {
                        this.mettreajour();
                    });
                }
                else
                {
                    return commandMiseaJour;
                }
            }
        }

        private ICommand commandMiseaJour { get; set; }
    }
}
