﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfReservationHotel.viewmodels;

namespace WpfReservationHotel.views
{
    /// <summary>
    /// Logique d'interaction pour ControlReservations.xaml
    /// </summary>
    public partial class ControlReservations : UserControl
    {

        public ControlReservations()//ViewModelReservations viewModelReservations)
        {
            InitializeComponent();
            DataContext = new ViewModelReservations();

        }
    }
}
