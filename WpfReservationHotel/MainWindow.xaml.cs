﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfReservationHotel.viewmodels;
using WpfReservationHotel.models;
using System.Resources;
using WpfReservationHotel.Properties;
using System.IO;
using System.Windows.Markup;
using System.Windows.Forms;
using WpfReservationHotel.views;
using System.Threading;
using System.Web.UI.WebControls;
using Application = System.Windows.Application;

namespace WpfReservationHotel
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Thread MainThread { get; set; }
        ViewModelMain MainModel { get; set; }

        ResourceManager ResourcesManager { get { try { return new ResourceManager(typeof(Resources)); }
                catch(Exception e){ return null; }
            } }
        public MainWindow()
        {
            
            InitializeComponent();

            MainModel = new ViewModelMain(new ViewModelMenu());
             //on s'inscrit aux notifications de changement de connexion
            this.MainModel.OnConnexionReussie += MainModel_OnConnexionReussie;
            this.MainModel.OnDeconnexionReussie += MainModel_OnDeconnexionReussie;
            DataContext = MainModel;
            //on met à jour le viewmodelmenu dans viewmodelmain afin que les deconnexions puissent etre observes
            this.onUtilisateurDeconnecte();
            //MainModel.updateViewModelMenu(MenuView.DataContext as ViewModelMenu);

        }

        private void MainModel_OnDeconnexionReussie(object sender, EventArgs e)
        {
            this.onUtilisateurDeconnecte();
        }

        private void MainModel_OnConnexionReussie(object sender, EventArgs e)
        {
            string role = MainModel.ViewModelConnexion.RoleUtilisateur.libelle;
            Console.WriteLine(role);
            this.onUtilisateurConnecte(role);
        }

        public void onUtilisateurDeconnecte() {
            ControlConnexion control = new ControlConnexion();
            MainView.Content = control;
            MainModel.updateViewModelConnexion(control.DataContext as ViewModelConnexion); 

        }
        public void onUtilisateurConnecte(string role) {
            ResourceManager managerResources = this.ResourcesManager;
            //Task.RunSynchronously()

            if (managerResources != null)           

            {
                string roleMenage="";
                string roleResaturation="";
                
                    roleMenage = managerResources.GetString("roleMenage");
                    roleResaturation = managerResources.GetString("roleRestaurant");
                

                if (role.Equals(roleMenage))
                {
                    ChargerMenage();
                }
                else if (role.Equals(roleResaturation))
                {
                    ChargerRestauration();
                }
                else
                {

                    ChargerReservations();
                }
            }
        }

        public void ChargerMenage()
        {
            MainView.Content = new ControlMenage();

        }

        public void ChargerRestauration()
        {
            MainView.Content = new ControlRestauration();
        }

        public void ChargerReservations()
        {
            MainView.Content = new ControlMainReservations();

        }




    }
}
